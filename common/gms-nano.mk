# Soong namespace
PRODUCT_SOONG_NAMESPACES += \
	vendor/gms/common \
	vendor/gms/extras \
	vendor/gms/overlay

# Configurations
PRODUCT_COPY_FILES += \
	vendor/gms/common/proprietary/product/app/Chrome/Chrome.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/Chrome/Chrome.apk.gz \
	vendor/gms/common/proprietary/product/app/TrichromeLibrary/TrichromeLibrary.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/TrichromeLibrary/TrichromeLibrary.apk.gz \
	vendor/gms/common/proprietary/product/app/WebViewGoogle/WebViewGoogle.apk.gz:$(TARGET_COPY_OUT_PRODUCT)/app/WebViewGoogle/WebViewGoogle.apk.gz \
	vendor/gms/common/proprietary/product/etc/default-permissions/default-permissions.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/default-permissions/default-permissions.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.android.omadm.service.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.android.omadm.service.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.android.apps.dialer.call_recording_audio.features.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.apps.dialer.call_recording_audio.features.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.android.dialer.support.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.dialer.support.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.android.odad.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.odad.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.android.odad.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.odad.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.omadm.trigger.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.omadm.trigger.xml \
	vendor/gms/common/proprietary/product/etc/permissions/com.google.omadm.trigger.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.omadm.trigger.xml \
	vendor/gms/common/proprietary/product/etc/permissions/privapp-permissions-google-p.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-google-p.xml \
	vendor/gms/common/proprietary/product/etc/permissions/split-permissions-google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/split-permissions-google.xml \
	vendor/gms/common/proprietary/product/etc/preferred-apps/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/preferred-apps/google.xml \
	vendor/gms/common/proprietary/product/etc/security/fsverity/gms_fsverity_cert.der:$(TARGET_COPY_OUT_PRODUCT)/etc/security/fsverity/gms_fsverity_cert.der \
	vendor/gms/common/proprietary/product/etc/security/fsverity/play_store_fsi_cert.der:$(TARGET_COPY_OUT_PRODUCT)/etc/security/fsverity/play_store_fsi_cert.der \
	vendor/gms/common/proprietary/product/etc/sysconfig/game_service.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/game_service.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/google-staged-installer-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google-staged-installer-whitelist.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/google.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/google_build.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google_build.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/google_searcle.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/google_searcle.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/nexus.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/nexus.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/pixel_2016_exclusive.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_2016_exclusive.xml \
	vendor/gms/common/proprietary/product/etc/sysconfig/quick_tap.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/quick_tap.xml \
	vendor/gms/common/proprietary/system/etc/permissions/privapp-permissions-google.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-google.xml \
	vendor/gms/common/proprietary/system_ext/etc/permissions/privapp-permissions-google-se.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-google-se.xml

# GMS mandatory core packages
PRODUCT_PACKAGES += \
	AndroidPlatformServices \
	ConfigUpdater \
	GoogleExtShared \
	GoogleOneTimeInitializer \
	GoogleRestorePrebuilt \
	GoogleServicesFramework \
	PartnerSetupPrebuilt \
	Phonesky \
	PrebuiltGmsCoreSc \
	SetupWizardPrebuilt \
	WebViewGoogle-Stub

# GMS mandatory application packages
PRODUCT_PACKAGES += \
	Chrome-Stub \
	MarkupGoogle \
	Photos \
	TrichromeLibrary-Stub

# GMS optional application packages
PRODUCT_PACKAGES += \
	CalendarGooglePrebuilt \
	GoogleContacts \
	GoogleDialer \
	PrebuiltBugle \
	WallpaperPickerGoogleRelease \
	com.google.android.dialer.support

# PixelSetupWizard (Android 11 - coral)
PRODUCT_PACKAGES += \
	PixelSetupWizardCoral \

# PixelSetupWizard properties
PRODUCT_PRODUCT_PROPERTIES += \
	ro.setupwizard.enterprise_mode=1 \
	ro.setupwizard.esim_cid_ignore=00000001 \
	ro.setupwizard.rotation_locked=true \
	setupwizard.feature.baseline_setupwizard_enabled=true \
	setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
	setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
	setupwizard.theme=glif_v3_light \
	setupwizard.feature.show_pixel_tos=true \
	setupwizard.feature.show_support_link_in_deferred_setup=false \
	setupwizard.enable_assist_gesture_training=false
