# Extracted from:
# Google - Redfin (Android 12) - for AndroidSystemIntelligence
# Google - cheetah (Android 13)
# Nokia - snt_sprout (Android 13)

# GMS core packages - Nokia
product/app/GoogleLocationHistory/GoogleLocationHistory.apk;PRESIGNED
product/app/WebViewGoogle/WebViewGoogle.apk;OVERRIDES=webview;PRESIGNED
product/etc/security/fsverity/gms_fsverity_cert.der
product/etc/security/fsverity/play_store_fsi_cert.der
product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
product/priv-app/GmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
product/priv-app/GmsCore/GmsCore.apk;OVERRIDES=NetworkRecommendation;PRESIGNED
product/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;OVERRIDES=OneTimeInitializer;PRESIGNED
product/priv-app/GooglePartnerSetup/GooglePartnerSetup.apk;PRESIGNED
product/priv-app/GoogleRestore/GoogleRestore.apk;PRESIGNED
product/priv-app/Phonesky/Phonesky.apk;PRESIGNED
product/priv-app/Wellbeing/Wellbeing.apk;PRESIGNED
system/system/app/GoogleExtShared/GoogleExtShared.apk;PRESIGNED
system/system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;OVERRIDES=PrintRecommendationService;PRESIGNED
system/system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk;OVERRIDES=PackageInstaller;PRESIGNED
system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED
system_ext/priv-app/SetupWizard/SetupWizard.apk;OVERRIDES=Provision;PRESIGNED

# GMS web packages - Nokia
product/app/Chrome/Chrome.apk;OVERRIDES=Browser,Browser2,Jelly;PRESIGNED
product/app/TrichromeLibrary/TrichromeLibrary.apk;PRESIGNED
product/priv-app/Velvet/Velvet.apk;OVERRIDES=QuickSearchBox;PRESIGNED
system/system/app/ChromeCustomizations/ChromeCustomizations.apk;PRESIGNED
system/system/lib/libchrome.so
system/system/lib64/libchrome.so
vendor/lib64/libchrome.so

# GMS comms suite (mandatory) - GooglePixel
product/app/GoogleContacts/GoogleContacts.apk;OVERRIDES=Contacts;PRESIGNED
product/etc/permissions/com.google.android.dialer.support.xml
product/framework/com.google.android.dialer.support.jar;PRESIGNED
product/priv-app/GoogleDialer/GoogleDialer.apk;OVERRIDES=Dialer;PRESIGNED
product/priv-app/PrebuiltBugle/PrebuiltBugle.apk;OVERRIDES=messaging;PRESIGNED

# GMS packages (Optional) - Nokia
product/app/CalculatorGoogle/CalculatorGoogle.apk;OVERRIDES=Calculator,ExactCalculator;PRESIGNED
product/app/CalendarGoogle/CalendarGoogle.apk;OVERRIDES=Calendar,GoogleCalendarSyncAdapter,Etar;PRESIGNED
product/app/DeskClockGoogle/DeskClockGoogle.apk;OVERRIDES=AlarmClock,DeskClock;PRESIGNED
product/app/Gmail2/Gmail2.apk;PRESIGNED
product/app/LatinImeGoogle/LatinImeGoogle.apk;OVERRIDES=LatinIME;PRESIGNED
product/app/Maps/Maps.apk;PRESIGNED
product/app/Photos/Photos.apk;OVERRIDES=Gallery2,SnapdragonGallery;PRESIGNED
product/app/YouTube/YouTube.apk;PRESIGNED
product/app/talkback/talkback.apk;PRESIGNED
product/priv-app/FilesGoogle/FilesGoogle.apk;PRESIGNED
product/priv-app/Turbo/Turbo.apk;PRESIGNED
system/system/app/PlayAutoInstallStub/PlayAutoInstallStub.apk;PRESIGNED

# GMS packages (Pixel Experience) - GooglePixel
product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
product/app/PixelThemesStub/PixelThemesStub.apk;OVERRIDES=ThemePicker;PRESIGNED
product/app/PixelThemesStub2022_and_newer/PixelThemesStub2022_and_newer.apk;PRESIGNED
product/app/PixelWallpapers2022/PixelWallpapers2022.apk;PRESIGNED
product/app/SoundPickerPrebuilt/SoundPickerPrebuilt.apk;PRESIGNED
product/app/WallpaperEmojiPrebuilt/WallpaperEmojiPrebuilt.apk;PRESIGNED
product/priv-app/DeviceIntelligenceNetworkPrebuilt/DeviceIntelligenceNetworkPrebuilt.apk;PRESIGNED
product/priv-app/DevicePersonalizationPrebuiltPixel2020/DevicePersonalizationPrebuiltPixel2020.apk;PRESIGNED|d8cf5dcc17027257149da5fe0781f2c421b612c6
product/priv-app/PixelLiveWallpaperPrebuilt/PixelLiveWallpaperPrebuilt.apk;PRESIGNED
product/priv-app/RecorderPrebuilt/RecorderPrebuilt.apk;PRESIGNED
product/priv-app/SafetyHubPrebuilt/SafetyHubPrebuilt.apk;PRESIGNED
product/priv-app/ScribePrebuilt/ScribePrebuilt.apk;PRESIGNED
product/priv-app/SecurityHubPrebuilt/SecurityHubPrebuilt.apk;PRESIGNED
product/priv-app/SettingsIntelligenceGooglePrebuilt/SettingsIntelligenceGooglePrebuilt.apk;OVERRIDES=SettingsIntelligence;PRESIGNED
system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk;OVERRIDES=DocumentsUI;PRESIGNED
system_ext/app/EmergencyInfoGoogleNoUi/EmergencyInfoGoogleNoUi.apk;OVERRIDES=EmergencyInfo
system_ext/priv-app/NexusLauncherRelease/NexusLauncherRelease.apk;OVERRIDES=Launcher3QuickStep,TrebuchetQuickStep;PRESIGNED
system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk;OVERRIDES=LineageSetupWizard;PRESIGNED
system_ext/priv-app/StorageManagerGoogle/StorageManagerGoogle.apk;OVERRIDES=StorageManager;PRESIGNED
system_ext/priv-app/WallpaperPickerGoogleRelease/WallpaperPickerGoogleRelease.apk;OVERRIDES=WallpaperPicker,WallpaperPicker2,WallpaperCropper;PRESIGNED

# Configuration files [Permissions] - Nokia
product/etc/permissions/privapp-permissions-google-product.xml
product/etc/sysconfig/google-staged-installer-whitelist.xml
system/system/etc/permissions/privapp-permissions-google-system.xml
system_ext/etc/permissions/privapp-permissions-google-system-ext.xml

# Configuration files (Permissions & unlock the features) - GooglePixel
product/etc/default-permissions/default-permissions.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/permissions/split-permissions-google.xml
product/etc/preferred-apps/google.xml
product/etc/sysconfig/game_service.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google-install-constraints-package-allowlist.xml
product/etc/sysconfig/google-staged-installer-whitelist.xml
product/etc/sysconfig/google.xml
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/pixel_2016_exclusive.xml|f92443f76f9af566813e08495f00c37a882af824
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2021.xml
product/etc/sysconfig/pixel_experience_2021_midyear.xml
product/etc/sysconfig/pixel_experience_2022.xml
product/etc/sysconfig/pixel_experience_2022_midyear.xml
product/etc/sysconfig/preinstalled-packages-product-pixel-2017-and-newer.xml
product/etc/sysconfig/quick_tap.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml
